import React, { useCallback, useEffect, useState } from "react";
// @ts-ignore
import Footer from "./Footer.tsx";
// @ts-ignore
import Body from "./Body.tsx";
// @ts-ignore
import Header from "./Header.tsx";
import "./TextPage.scss";
// @ts-ignore
import AI from "../AI/AI.tsx";
// @ts-ignore
import LocalStorageService from "../services/LocalStorageService.tsx";
// @ts-ignore
import GoogleApi from "../services/GoogleApi.tsx";

export enum Recipient {
    AI, USER
}

export interface Message {
    message: string;
    from: Recipient;
    id: string;
}

const scrollToBottom = () => {
    setTimeout(() => {
        const elem = document.getElementById("body");
        if (elem) {
            elem.scrollTop = elem.scrollHeight;
        }
    }, 0);
}

const TextPage: React.FC = () => {

    const [ messages, setMessages ] = useState<Message[]>(LocalStorageService.getMessages());
    const [isAiTyping, setIsAiTyping] = useState(false);
    const userId = LocalStorageService.getId();
    
    const sendMessage = useCallback((message: Message) => {
        setIsAiTyping(false);
        setMessages([...messages, message]);
        // LocalStorageService.storeMessages([...messages, message]);
        GoogleApi.setMessages(userId, [...messages, message]);
        scrollToBottom();
    }, [messages, userId]);

    useEffect(() => {
        if (messages.length === 0) {
            return;
        }
        const recentMessage = messages[messages.length -1];
        if (recentMessage.from === Recipient.USER) {

            const aiResponse = AI.getResponse(recentMessage.message);
            const aiTypingDelay = 500;
            const delay = aiTypingDelay + aiResponse.length*(40 + 60*(Math.random()));

            setTimeout(() => {
                setIsAiTyping(true);
                scrollToBottom();
            }, aiTypingDelay);
            setTimeout(() => sendMessage({from: Recipient.AI, message: aiResponse, id: 'ai'}), delay);
        }
    }, [messages, sendMessage]);

    useEffect(() => {
        scrollToBottom();
    }, []);

    return (
        <div id="textPage">
            <button onClick={() => {
                GoogleApi.getMessages(userId);
            }}></button>
            <Header />
            <Body messages={messages} isAiTyping={isAiTyping}/>
            <Footer sendMessage={sendMessage} id={userId}/>
        </div>
    );
}

export default TextPage;
