import React, { useState } from "react";
import "./Footer.scss";
// @ts-ignore
import { Message, Recipient } from "./TextPage.tsx";

interface FooterProps {
    sendMessage: (message: Message) => void;
    id: string;
}

const Footer: React.FC<FooterProps> = ({ sendMessage, id }) => {

    const [ message, setMessage ] = useState("");
    
    const sendText = () => {
        sendMessage({ from: Recipient.USER, message: message, id: id });
        setMessage("");
    }

    const onKeyDown = (key: string) => {
        if (key === "Enter") {
            sendText();
        }
    }

    return (
        <div id="footer">
            <input 
                type="text"
                id="input"
                onChange={(event) => setMessage(event.target.value)}
                value={message}
                onKeyDown={(event) => onKeyDown(event.key)}            
            />
        </div>
    );
}

export default Footer;