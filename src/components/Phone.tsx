import React from "react";
// @ts-ignore
import TextPage from "./TextPage.tsx";
import "./Phone.scss";

const Phone: React.FC = () => {
    return (
        <div id="phone">
            <TextPage />
        </div>
    );
}

export default Phone;
