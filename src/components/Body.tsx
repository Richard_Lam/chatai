import React from "react";
import "./Body.scss";
// @ts-ignore
import { Message, Recipient } from "./TextPage.tsx";

interface BodyProps {
    messages: Message[];
    isAiTyping: boolean;
}

const Body: React.FC<BodyProps> = ({ messages, isAiTyping }) => {

    return (
        <div id="body">
            {messages.map((message) => {
                return (
                    <div
                        className={`message ${message.from === Recipient.AI ? "ai" : "user"}`}
                        key={`message-` + (Math.random()*10000)}
                    >
                        {message.message}
                    </div>
                );
            })}
            {isAiTyping ?
                <div className={'message'}>
                    <div className="typing-indicator">
                        <div />
                        <div />
                        <div />
                    </div>
                </div>
            : null}
        </div>
    );
}

export default Body;