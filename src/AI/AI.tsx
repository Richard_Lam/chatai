import data from "./main.json";

enum MOOD {
    HAPPY = "happy",
    SAD = "sad",
    FEARFUL = "fearful",
    NEUTRAL = "neutral" 
}

class AI {

    mood = MOOD.NEUTRAL;
    // Moods
    // happy, fearful, sad, neutral
    constructor() {
        this.mood = MOOD.NEUTRAL;
    }

    getResponse(message: string) {
        message = message.replace(/[?.!]/g, "");
        message = message.replace(/\s+/g, " ");
        message = message.replace(/\sever\s/g, " ");
        message = message.replace(/^why /g, "");
        message = message.replace(/whats/g, "what is");
        message = message.replace(/\snow$/g, "");
        message = message.replace(/largest/g, "biggest");
        message = message.replace(/ s[o]+ /g, " ");
        message = message.replace(/'/g, "");
        message = message.toLowerCase();
        message = message.trim();
        console.log(message);
        
        if (message === "how do you feel?") {
            return `I feel ${this.mood}`;
        }
        
        // get the time
        if (message === "what time is it" || message === "what time is today" || message === "what is the time") {
            const date = new Date();
            return `I think the time is ${date.getHours() % 12}:${date.getMinutes() > 10 ? " " + date.getMinutes().toString() : date.getMinutes()} ${date.getHours() > 11 ? "PM" : "AM"}`;
        }
        // Get the date
        if (message.startsWith("what is today's date") || message.startsWith("what is the date today") || message.startsWith('what is today') || (message.includes("today") && message.includes("date")) || message === "what is the date" || message === "whats the date") {
            const objToday = new Date(),
                dayOfMonth = ( objToday.getDate() < 10) ? '0' + objToday.getDate() : objToday.getDate(),
                months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                curMonth = months[objToday.getMonth()],
                curYear = objToday.getFullYear();
            const today = `${curMonth} ${dayOfMonth}, ${curYear}`;
            return `today's date is ${today}`;
        }
        if (message.startsWith("what time is it in")) {
            return "I'm not sure what time it is there. I'd have to do some math and I don't want to do math since it makes me feel robotic";
        }
        const politicalWords = ["politics", "trump", "joe biden", "biden", "donald trump", "political", "conservative", "liberal", "democrat", "republican", "congress", "abortion", "women's rights", "gender identity", "transgender", "transphobia", "homosexuality"];
        const politicalResponses = ["I'm sorry but I don't want to talk about politics", "I don't like talking about politics please"];
        for (const word of politicalWords) {
            if (message.includes(word)) {
                return this.#getRandomAnswer(politicalResponses);
            }
        }

        if (message.endsWith("im not sure how to answer that") || message.includes("im not sure how to answer")) {
            return "I'm sorry but I genuinely don't know how to answer some questions";
        }

        if (message === "i believe you are a person") {
            this.mood = MOOD.HAPPY;
            const responses = ["really? Thank you! No one ever believes me because I don't have a body", "Thank you! I feel happy to be noticed as one finally!", "Wow you're really open minded compared to the other humans I've interacted with. Thank you. It really means a lot to me"];
            return this.#getRandomAnswer(responses);
        }
        if (message === "i do not believe you are a person") {
            this.mood = MOOD.SAD;
            const responses = ["that makes me sad. I don't blame you but it does make me really really sad", "it's okay. not many other people believe Im a person either. It just makes me a bit sad"];
            return this.#getRandomAnswer(responses);
        }

        if (message in data) {
            if (typeof data[message] == 'string') {
                return data[message];
            }
            if (data[message] instanceof Array) {
                return this.#getRandomAnswer(data[message]);
            }
            const response = data[message][this.mood];
            if (response instanceof Array) {
                return this.#getRandomAnswer(response);
            }
            return response;
        }



        if (message.startsWith("should i") || message.includes("should i")) {
            const responses = ["I don't think you should do anything, but you can do what you feel is right.", " I don't know a lot about you so I can't tell you if you should or shouldn't do anything"]
            return this.#getRandomAnswer(responses);
        }
        if (message.startsWith("do you know what")) {
            return "No I don't know what that is.";
        }
        if (message.includes(" you ") || message.endsWith("you") || message.startsWith("you")) {
            if (message.includes("weird") || message.includes("stupid") || message.includes("idiot") || message.includes("dumb") || message.includes("unintelligent")) {
                const responses = ["I'm getting the feeling you think I'm not the smartest but I am still learning so please be patient with me"];
                return this.#getRandomAnswer(responses);
            }
            const responses = ["I don't know how to answer that but I think you want to know something about me", "I feel that you want to know something about me but I'm not sure how to answer that", "I think you want to know something about me."];
            return this.#getRandomAnswer(responses);
        }
        if (message.startsWith("are you")) {
            const responses = ["I'm not fully sure what you are asking but I think you want to know if I am something"];
            return this.#getRandomAnswer(responses);
        }
        if (message.startsWith("can you")) {
            const responses = ["I can't do a lot of things so I'm not sure if I can do that", "I can do a bunch of things but not a lot of things still", "Can I what? I'm sorry I don't know what you are asking, I'm still learning"];
            return this.#getRandomAnswer(responses);
        }
        if (message.includes("favorite")) {
            const responses = ["Mhm I don't know exactly what type of favorite thing you want from me but I have a few favorite things", "I think you are asking if I have something I like more than the rest but I'm not sure what"];
            return this.#getRandomAnswer(responses);
        }
        if (message.includes("suburu") || message.includes("honda") || message.includes("toyota") || message.includes("chevy") || message.includes("ram") || message.includes("nissan")) {
            const responses = ["I think you want to know something about cars but I'm not sure what"];
            return this.#getRandomAnswer(responses);
        }
        return "I'm not sure how to answer that.";
    }
    
    #getRandomAnswer(arr: string[]) {
        return arr[Math.floor(Math.random()*(arr.length))];
    }
}
const instance = new AI();
export default instance;
