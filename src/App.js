import Phone from "./components/Phone.tsx";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <Phone />
    </div>
  );
}

export default App;
