// @ts-ignore
import { Message } from "../components/TextPage.tsx";

class LocalStorageService {

    #messagesKey = "messages";
    #idKey = "id";

    getMessages() {
        const obj = localStorage.getItem(this.#messagesKey);
        if (obj == null) {
            const emptyArr: Message[] = [];
            return emptyArr;
        }
        return JSON.parse(obj);
    }
    storeMessages(messages: Message[]) {
        localStorage.setItem(this.#messagesKey, JSON.stringify(messages));
    }
    getId() {
        const obj = localStorage.getItem(this.#idKey);
        if (obj == null) {
            const id = `${Math.random()*1000000000}`
            localStorage.setItem(this.#idKey, id);
        }
        return localStorage.getItem(this.#idKey);
    }
}

const instance = new LocalStorageService();
export default instance;
