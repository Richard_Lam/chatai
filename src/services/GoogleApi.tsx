// @ts-ignore
import { Message } from "../components/TextPage.tsx";
import axios from "axios";

class GoogleApi {

    async getMessages(id: string) {
        const response = await axios.get("https://sheets.googleapis.com/v4/spreadsheets/1JGhdEAU8pkK8SfNEBJ5bu0q0-Q19q0MtcTPyeMpaGnM/values/A:B?key=AIzaSyBU_8lFztilGN9nKYcgUH4FGlyJ2L59poM");
        if (response) {
            const values: string[][] = response.data.values;
            values.forEach((row) => {
                if (row[0] === id) {
                    console.log(JSON.parse(row[1]));
                    return JSON.parse(row[1]);
                }
            });
        }
        const emptyArr: Message[] = [];
        return emptyArr;
    }
    async setMessages(id: string, messages: Message[]) {
        // const token = "ya29.A0AVA9y1vUc99kbx5Pbb2iUC-byYWlBmVEBeI6aGOmypsTmtZA6JRoX260O-FRA-FvJSDU68GKQngfGYkdLWpHQZzh4HnXxN3rlasRn_wNMhJza3EU4NL7EzncVVFuU9eZOACT6iZpiMyYvRczM-PwN8yZUJFEaCgYKATASATASFQE65dr8GeuKRXU1yVXCYrrtdSZeKA0163";
        // const fromId = "0";
        // let response = await axios.get("https://sheets.googleapis.com/v4/spreadsheets/1JGhdEAU8pkK8SfNEBJ5bu0q0-Q19q0MtcTPyeMpaGnM/values/A:B?key=AIzaSyBU_8lFztilGN9nKYcgUH4FGlyJ2L59poM");
        // if (response) {
        //     const values: string[][] = response.data.values;
        //     values.forEach((row) => {
        //         if (row[0] === id) {
        //             const data = JSON.parse(row[1]);
        //             data[fromId] = messages;
        //             row[1] = JSON.stringify(messages);
        //         }
        //     });
        //     response = await axios.post("https://sheets.googleapis.com/v4/spreadsheets/1JGhdEAU8pkK8SfNEBJ5bu0q0-Q19q0MtcTPyeMpaGnM/values:batchUpdate", 
        //     {
        //         body: '{"valueInputOption":"RAW","data":[{"range":"A1:B1","majorDimension":"ROWS","values":[["google api ran!", "another one ran!"]]}],"includeValuesInResponse": false,"responseValueRenderOption": "FORMATTED_VALUE"}'
        //     },            
        //     {
        //         headers: {
        //             Authorization: 'Bearer ' + token 
        //         }
        //     });
        // }
    }
}

const instance = new GoogleApi();
export default instance;
